#!/bin/bash

pass="xxxxxx"

fts_backup_file=/backup/$(date +%F)_config_fts_DB.sql

mysqldump --single-transaction -h itrac5152.cern.ch  -P 5503 -u admin  -p$pass newfts3lb t_activity_share_config t_authz_dn t_bad_dns t_bad_ses t_cloudStorage t_cloudStorageUser t_config_audit t_credential t_credential_cache  t_debug t_file_retry_errors t_optimizer t_group_members t_hosts t_link_config t_optimize t_optimize_active t_optimize_mode t_optimize_streams t_schema_vers t_se  t_server_config  t_share_config t_stage_req t_vo_acl >  $fts_backup_file

tar zcvf ${fts_backup_file}.tar.gz $fts_backup_file

rm -f $fts_backup_file 

find /backup/*.tar.gz -mtime +90 -exec rm {} \;

