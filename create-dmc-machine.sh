#!/bin/bash

export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_PROJECT_NAME="IT DMC"
export OS_AUTH_URL=https://keystone.cern.ch/v3
export OS_USERNAME=$USER



openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server not working"
  exit
fi

export ENVIRO="qa"
export IMAGE='CC7 - x86_64 [2018-01-12]'

KEYPAIR="fts3ops"

NUMBER=$1

STRING="dmc-build-$NUMBER"
    ai-bs \
        --foreman-hostgroup 'dmc/build' \
         --foreman-environment "${ENVIRO}" \
         --landb-mainuser 'DMC-DEVEL' \
         --landb-responsible 'DMC-DEVEL' \
         --nova-flavor "m2.large" \
         --nova-sshkey "${KEYPAIR}" \
         --nova-image "${IMAGE}" \
     $STRING
