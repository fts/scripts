#!/usr/bin/python3

# ----------------------------------------------------------------------------------------
# File: fts-token-compliance.py
# Author: Steven Murray - CERN
# Author: Mihai Patrascoiu - CERN
#
# Description: Given the FTS client configuration, performs the "token-exchange"
#              and "refresh-token" grants. Should these operations work, there's
#              a very high chance the TokenProvider will also work with FTS
# ----------------------------------------------------------------------------------------

import argparse
import base64
import configparser
import json
import os
import sys
import requests

from datetime import datetime


def decode_token(raw):
    split_raw = raw.split('.')

    if len(split_raw) != 3:
        raise Exception("Could not split token into three parts using '.' as the delimiter")

    encoded_header = split_raw[0]
    encoded_payload = split_raw[1]

    for i in range(0, len(encoded_header) % 4):
        encoded_header += '='
    for i in range(0, len(encoded_payload) % 4):
        encoded_payload += '='

    return {
        "raw": raw,
        "header": json.loads(base64.urlsafe_b64decode(encoded_header)),
        "payload": json.loads(base64.urlsafe_b64decode(encoded_payload)),
        "signature": split_raw[2]
    }


def get_openid_configuration(issuer):
    url = f"{issuer}.well-known/openid-configuration"
    response = requests.get(url)

    if response.status_code != 200:
        raise Exception(
            f"Failed to get OpenID configuration from {issuer}: "
            f"status_code={response.status_code}"
        )

    try:
        return json.loads(response.text)
    except Exception as e:
        raise Exception(f"Failed to parse JSON response from {url}: {e}")


def perform_token_exchange(token_endpoint, client_id, client_secret, access_token):
    aud = access_token["payload"].get("aud", None)
    scope = access_token["payload"].get("scope", None)
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    data = {
        "grant_type": "urn:ietf:params:oauth:grant-type:token-exchange",
        "requested_token_type": "urn:ietf:params:oauth:token-type:refresh_token",
        "subject_token_type": "urn:ietf:params:oauth:token-type:access_token",
        "subject_token": access_token["raw"]
    }
    if scope:
        data["scope"] = scope
    if aud:
        data["audience"] = aud

    response = requests.post(
        token_endpoint,
        headers=headers,
        data=data,
        auth=(client_id, client_secret)
    )

    if response.status_code != 200:
        raise Exception(
            f"Failed to perform 'token-exchange' grant: status_code={response.status_code} text={response.text}")

    json_response = json.loads(response.text)

    if "refresh_token" not in json_response:
        raise Exception(
            f"Failed to perform 'token-exchange' grant: No refresh token in the response\n{json_response}")

    refresh_token = decode_token(json_response["refresh_token"])
    exchanged_access_token = None

    if "access_token" in json_response:
        exchanged_access_token = decode_token(json_response["access_token"])

    return exchanged_access_token, refresh_token


def perform_token_refresh(token_endpoint, client_id, client_secret,
                          access_token, refresh_token):
    aud = access_token["payload"].get("aud", None)
    scope = access_token["payload"].get("scope", None)
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    data = {
        "grant_type": "refresh_token",
        "refresh_token": refresh_token["raw"],
    }
    if scope:
        data["scope"] = scope
    if aud:
        data["audience"] = aud

    response = requests.post(
        token_endpoint,
        headers=headers,
        data=data,
        auth=(client_id, client_secret)
    )

    if response.status_code != 200:
        raise Exception(
            f"Failed to perform 'refresh_token' grant: status_code={response.status_code} text={response.text}")

    json_response = json.loads(response.text)

    if "access_token" not in json_response:
        raise Exception(
            f"Failed to perform 'refresh_token' grant: No access token in the response\n{json_response}")

    return decode_token(json_response["access_token"])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Exercise the FTS token compliance steps",
                                     add_help=True, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument(dest="config", help="The path to the token configuration file")
    args = parser.parse_args()

    if not os.path.isfile(args.config):
        print(f"The configuration file does not exist: path={args.config}")
        sys.exit(1)

    config = configparser.ConfigParser()
    config.read(args.config)

    issuer = config.get("fts", "issuer")
    if not issuer.endswith('/'):
        issuer += '/'

    openid_configuration = get_openid_configuration(issuer)
    fts_client_id = config.get("fts", "client_id")
    fts_client_secret = config.get("fts", "client_secret")

    access_token_file = config.get("token", "access_token_file")

    if "registration_endpoint" not in openid_configuration:
        print(f"'registration_endpoint' not found in the issuer OpenId Configuration! (issuer={issuer})")
        sys.exit(1)

    if "token_endpoint" not in openid_configuration:
        print(f"'token_endpoint' not found in the issuer OpenId Configuration! (issuer={issuer}")
        sys.exit(1)

    with open(access_token_file, 'r') as file:
        user_access_token = decode_token(file.read().rstrip())

    print("--------------------------------")
    print(f"[{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Performing 'token-exchange' grant...")
    print("--------------------------------")

    access_token, refresh_token = perform_token_exchange(
        token_endpoint=openid_configuration["token_endpoint"],
        client_id=fts_client_id,
        client_secret=fts_client_secret,
        access_token=user_access_token
    )

    if access_token is None:
        print("Access token:\n'token-exchange' response did not return any 'access_token'\n")
        access_token = user_access_token
    else:
        print(f"Access token:\n{json.dumps(access_token['payload'], indent=2)}\n")
    print(f"Refresh token:\n{json.dumps(refresh_token['payload'], indent=2)}\n")

    print("--------------------------------")
    print(f"[{datetime.now().strftime('%Y-%m-%d %H:%M:%S')}] Performing 'refresh_token' grant...")
    print("--------------------------------")

    access_token = perform_token_refresh(
        token_endpoint=openid_configuration["token_endpoint"],
        client_id=fts_client_id,
        client_secret=fts_client_secret,
        access_token=access_token,
        refresh_token=refresh_token
    )

    print(f"Access token:\n{json.dumps(access_token['payload'], indent=2)}\n")
    print("Success!")
