#!/bin/sh
if test -z "${FTS_END_POINT}"; then
  echo "The environment variable FTS_END_POINT needs to be set."
  exit 1
fi

./fts_stress_test \
  --fts-endpoint ${FTS_END_POINT} \
  --mock \
  --dst-qry-params time=120 \
  --nb-clients 10 \
  --nb-file-replicas 100 \
  --verbose
