#!/bin/sh
if test -z "${FTS_END_POINT}"; then
  echo "The environment variable FTS_END_POINT needs to be set."
  exit 1
fi

./fts_stress_test \
  --fts-endpoint ${FTS_END_POINT} \
  --src-file-url 'mock://src_se//src_file' \
  --src-qry-params 'staging_time=20' \
  --dst-dir-url 'mock://dst_se//dst_dir' \
  --dst-qry-params 'time=20' \
  --nb-clients 10 \
  --nb-file-replicas 100 \
  --bring-online 30 \
  --verbose
