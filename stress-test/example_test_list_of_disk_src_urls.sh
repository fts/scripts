#!/bin/sh

if test -z "${FTS_END_POINT}"; then
  echo "The environment variable FTS_END_POINT needs to be set."
  exit 1
fi

./fts_stress_test \
  --fts-endpoint ${FTS_END_POINT} \
  --src-file-list disk_src_urls.txt \
  --dst-dir-url 'mock://dst_se//dst_dir' \
  --dst-qry-params 'time=10' \
  --nb-clients 10 \
  --verbose
