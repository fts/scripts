# FTS Stress Testing

This directory currently contains the following simple python script that allows
you to submit a configurable number of FTS transfer requests to a given FTS
endpoint using a configurable number of client processes:

* `fts_stress_test`

At the time of writing this `README.md` file the script has the following
command-line options:
```
usage: fts_stress_test [-h] -e FTS_ENDPOINT
                       (-f SRC_FILE_URL | -l SRC_FILE_LIST)
                       [-n NB_FILE_REPLICAS] -d DST_DIR_URL -c NB_CLIENTS
                       [-b BRING_ONLINE | -a ARCHIVE_TIMEOUT] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -e FTS_ENDPOINT, --fts-endpoint FTS_ENDPOINT
                        The FTS endpoint.
  -f SRC_FILE_URL, --src-file-url SRC_FILE_URL
                        The URL of the single source file to be replicated to
                        the destination directory.
  -l SRC_FILE_LIST, --src-file-list SRC_FILE_LIST
                        File containing the list of source file URLs.
  -n NB_FILE_REPLICAS, --nb-file-replicas NB_FILE_REPLICAS
                        Number of times to replicate the single source file to
                        the destination. This option must and can only be used
                        with the -f option.
  -d DST_DIR_URL, --dst-dir-url DST_DIR_URL
                        The URL of the destination directory.
  -c NB_CLIENTS, --nb-clients NB_CLIENTS
                        The number of concurrent clients.
  -b BRING_ONLINE, --bring-online BRING_ONLINE
                        Bring online timeout in seconds.
  -a ARCHIVE_TIMEOUT, --archive-timeout ARCHIVE_TIMEOUT
                        Archive timeout in seconds.
  -v, --verbose         Switch on verbose output.
```
The script has two ways to specify the source file or files.  Firstly you can
specify a single source URL that will then be replicated many times to the
destination directory URL.  The base name of the destination URLs will be the
base name of the source URL plus an every increasing integer counter. Secondly
you can specify a list of unique source URLs.

This directory includes some examples of running the `fts_stress_test` script,
namely:

* `example_test_archive_list_of_disk_src_urls.sh`
* `example_test_archive_single_src_disk_file.sh`
* `example_test_list_of_disk_src_urls.sh`
* `example_test_list_of_tape_src_files.sh`
* `example_test_single_src_disk_file.sh`
* `example_test_single_src_tape_file.sh`

For these examples to work you need to set the `FTS_END_POINT` environment
variable and they need to be executed within this directory.

The scripts use the following two files as appropriate:

* `disk_src_urls.txt`
* `tape_src_urls.txt`
