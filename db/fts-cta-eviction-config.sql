--
-- FTS3 configuration for CTA eviction (requires FTS3 Schema v8.0.0)
--

-- EOSCTAATLAS
INSERT INTO t_se(storage, eviction) VALUES("root://eosctaatlas.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "root://eosctaatlas.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("https://eosctaatlas.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "https://eosctaatlas.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("davs://eosctaatlas.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "davs://eosctaatlas.cern.ch", eviction = 1;

-- EOSCTACMS
INSERT INTO t_se(storage, eviction) VALUES("root://eosctacms.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "root://eosctacms.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("https://eosctacms.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "https://eosctacms.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("davs://eosctacms.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "davs://eosctacms.cern.ch", eviction = 1;

-- EOSCTALHCB
INSERT INTO t_se(storage, eviction) VALUES("root://eosctalhcb.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "root://eosctalhcb.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("https://eosctalhcb.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "https://eosctalhcb.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("davs://eosctalhcb.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "davs://eosctalhcb.cern.ch", eviction = 1;

-- EOSCTAPUBLIC
INSERT INTO t_se(storage, eviction) VALUES("root://eosctapublic.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "root://eosctapublic.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("https://eosctapublic.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "https://eosctapublic.cern.ch", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("davs://eosctapublic.cern.ch", 1) ON DUPLICATE KEY UPDATE storage = "davs://eosctapublic.cern.ch", eviction = 1;

-- ANTARES
INSERT INTO t_se(storage, eviction) VALUES("root://antares.stfc.ac.uk", 1) ON DUPLICATE KEY UPDATE storage = "root://antares.stfc.ac.uk", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("https://antares.stfc.ac.uk", 1) ON DUPLICATE KEY UPDATE storage = "https://antares.stfc.ac.uk", eviction = 1;
INSERT INTO t_se(storage, eviction) VALUES("davs://antares.stfc.ac.uk", 1) ON DUPLICATE KEY UPDATE storage = "davs://antares.stfc.ac.uk", eviction = 1;
