#!/bin/bash
tmpfile=`mktemp`
pass=xxx
user=xxx
host=xxx
port=xxx
db=xxxx
timeout=1200
mail_recipients='fts-alarm@cern.ch'

mysql -u $user -h $host -P $port -p$pass $db -s -e "SELECT id, host, time, state,info FROM INFORMATION_SCHEMA.PROCESSLIST where COMMAND !='Sleep' order by time DESC LIMIT 1\G;" >$tmpfile
time_string=`grep time: $tmpfile`
time_query=$(echo $time_string| cut -f2 -d:)
echo $time_query
id_string=`grep id: $tmpfile`
id_query=$(echo $id_string| cut -f2 -d:)
subject="Killing FTS slow query"

if [ $time_query -gt $timeout ]; then
  echo "slow query, going to kill it"
  mysql -u $user -h $host -P $port -p$pass $db -s -e "KILL $id_query;"
  cat $tmpfile| mail -s "$subject" "$mail_recipients"    
else
  echo "everything  ok"
fi
rm $tmpfile

