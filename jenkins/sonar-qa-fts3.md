sonar-qa-fts3
=============

Repository: https://gitlab.cern.ch/fts/fts3.git  
Environment: Docker  
Output: `results.xml`

### Script

```bash
cd qa/
docker build --rm=true --pull --no-cache -f Dockerfile_mysql -t fts3-qa-mysql ..
docker build --rm=true --pull --no-cache -f Dockerfile -t fts3-qa ..

rm -rf /tmp/sonar-runner-2.4/
curl "https://repo1.maven.org/maven2/org/codehaus/sonar/runner/sonar-runner-dist/2.4/sonar-runner-dist-2.4.zip" > "/tmp/sonar-runner-dist-2.4.zip"
unzip "/tmp/sonar-runner-dist-2.4.zip" -d "/tmp"
cat > "/tmp/sonar-runner-2.4/conf/sonar-runner.properties" <<EOF
sonar.host.url=http://baluarte.cern.ch:8080
EOF
```
```bash
pushd qa

rm -rfv /tmp/fts3qa
mkdir -p /tmp/fts3qa
chmod g+s /tmp/fts3qa

env=`mktemp -d`
virtualenv --system-site-packages ${env}
source "${env}/bin/activate"
pip install --upgrade pip # Otherwise the next install will fail
pip install docker-compose

# First MySQL, and give it a while before FTS
docker-compose up -d fts3-qa-mysql
sleep 60
FTS3_HOST=`hostname -f` PROXY_PASSWD=${proxy_passwd} STOMP_PASSWORD=${stomp_passwd} FTS3_USER=`id -un` FTS3_GROUP=`id -gn` docker-compose up fts3-qa || true

docker-compose stop
docker-compose rm -vf
rm -rv "${env}"
popd

cp -v /tmp/fts3qa/coverage/*.xml .
```
```bash
export SONAR_RUNNER_OPTS="-Duser.timezone=+01:00 -Djava.security.egd=file:///dev/urandom"
/tmp/sonar-runner-2.4/bin/sonar-runner -X
```
```bash
docker rmi fts3-qa-mysql
docker rmi fts3-qa
```
