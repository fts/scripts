ca-letsencrypt
==============

Repository: https://gitlab.cern.ch/fts/ca_letsencrypt.git  
Output: RPMs

### Variables
```
repo_base=/eos/workspace/f/fts/repo/www/repos/testing
```

### Script

```bash
rm -rf build-utils/
git clone https://gitlab.cern.ch/fts/build-utils.git
```
```bash
RPMBUILD=`mktemp -d`
make mock RPMBUILD="${RPMBUILD}" MOCK_FLAGS="--verbose --configdir=build-utils/mock-config --resultdir=." MOCK_CHROOT=$chroot
rm -rfv "${RPMBUILD}"
```
```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

./build-utils/scripts/copy2repo --repo-base "${repo_base}" --mock-config "./build-utils/mock-config/${chroot}.cfg" *.rpm
```
