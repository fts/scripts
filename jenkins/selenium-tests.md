selenium-tests
==============

Repository: https://gitlab.cern.ch/fts/fts-rest.git  

### Variables
```
eos_user_cert=<concealed>
eos_user_key=<concealed>
password=<concealed>
```

### Script

```bash
# Copy cert and password-less key
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

cat > passwd <<EOF
${proxy_passwd}
EOF

rm -fv userkey.pem
export mypass=${password}
cp "${eos_user_cert}" ./usercert.pem

openssl rsa -in "${eos_user_key}" -passin "file:passwd" -out "userkey.pem"
openssl pkcs12 -export -out keyStore.p12 -inkey userkey.pem -in usercert.pem -password env:mypass
cp keyStore.p12 /home/jenkins/.mozilla/jenkins/

cat > password <<EOF
${password}
EOF

# Add user certificate
pk12util -d /home/jenkins/.mozilla/firefox/browser/default/jenkins -i /home/jenkins/.mozilla/jenkins/keyStore.p12 -w password
#certutil -A -d /home/jenkins/.mozilla/firefox/browser/default/jenkins -i /etc/grid-security/certificates/CERN-GridCA.pem -n CERN-GridCA -t T,T,T
#certutil -L -d /home/jenkins/.mozilla/firefox/browser/default/jenkins -i /etc/grid-security/certificates/CERN-Root-2.pem -n CERN-Root-2 -t T,T,T
#certutil -L -d home/jenkins/.mozilla/firefox/browser/default/jenkins

chmod 0400 ./userkey.pem
rm -fv passwd

# Set env
export X509_USER_CERT=~+/usercert.pem
export X509_USER_KEY=~+/userkey.pem
echo $X509_USER_CERT
echo $X509_USER_KEY

# Create proxy
voms-proxy-init --voms dteam:/dteam/Role=lcgadmin

# Download Geckodriver
wget https://github.com/mozilla/geckodriver/releases/download/v0.19.0/geckodriver-v0.19.0-linux64.tar.gz
tar -xvzf geckodriver-v0.19.0-linux64.tar.gz
chmod +x geckodriver

export PATH=$PATH:$(pwd)
export CDir=$(pwd)

export FTS3_HOST="fts3-devel.cern.ch"
cd src/fts3rest/fts3rest/tests/selenium

##########################################
# Run config/authorize -- good certificate
##########################################

#py.test --junitxml results.xml authorize/*
python authorize/test_add_good_certificate

python activity_shares/test_add_good_activity
python activity_shares/test_save_good_activity
python activity_shares/test_save_good_activity_add

python global/test_add_good_global
python global/test_save_good_global

python links/test_add_good_link
python links/test_add_good_share
python links/test_add_good_share_different_vo
python links/test_save_good_share

python storage/test_add_good_se_without_vo
python storage/test_add_good_se_with_vo
python storage/test_add_good_se_bad_vo
python storage/test_add_good_se_multiple_vo
python storage/test_save_good_se_without_vo
python storage/test_save_good_se_delete_vo
python storage/test_save_good_add_vo

##########################################
# Run config/authorize -- bad certificate
##########################################

python authorize/test_add_bad_certificate

python activity_shares/test_add_bad_activity
python activity_shares/test_save_bad_activity
python activity_shares/test_save_bad_activity_add

python global/test_add_bad_global
python global/test_save_bad_global

python links/test_add_bad_link
python links/test_add_bad_share

python storage/test_add_bad_se
python storage/test_save_bad_se_vo
python storage/test_save_bad_se

##########################################
# Delete everything
##########################################

python authorize/test_delete_certificate
python activity_shares/test_delete_activity
python links/test_delete_link
python links/test_delete_share
python storage/test_delete_se
python global/test_delete_global
```
```bash
cd $CDIR
rm -f geckodriver
```
