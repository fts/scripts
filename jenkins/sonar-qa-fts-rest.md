sonar-qa-fts-rest
=================

Repository: https://gitlab.cern.ch/fts/build-utils.git  

### Variables
```
gpg_passphrase=<concealed>
```

### Script

```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 --scrub all
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 install python-pip python-virtualenv python-setuptools gnupg2 gcc swig openssl-devel java-1.8.0-openjdk wget git unzip
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 install gfal2 gfal2-plugin-mock gfal2-python
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot 'wget "https://repo1.maven.org/maven2/org/codehaus/sonar/runner/sonar-runner-dist/2.4/sonar-runner-dist-2.4.zip"'
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot 'unzip -o "sonar-runner-dist-2.4.zip"'

cp /eos/workspace/f/fts/repo/qa/sonar-runner.properties.gpg .
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 --copyin "./sonar-runner.properties.gpg" /

/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot "gpg --batch --yes --passphrase ${gpg_passphrase} --output ./sonar-runner-2.4/conf/sonar-runner.properties -d sonar-runner.properties.gpg"
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot 'rm -rfv fts-rest'
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot 'git clone https://gitlab.cern.ch/fts/fts-rest.git -b develop'
/usr/bin/mock --disable-plugin=tmpfs --configdir=./mock-config/ -r dev-epel-7-x86_64 chroot 'cd /fts-rest/src/fts3rest; ./qa.sh; export SONAR_RUNNER_OPTS="-Duser.timezone=+01:00 -Djava.security.egd=file:///dev/urandom"; /sonar-runner-2.4/bin/sonar-runner -X'
```
