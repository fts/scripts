fts3-test-suite-pilot
=====================

Repository: https://gitlab.cern.ch/fts/fts-config-collector.git  

### Variables
```
login=<concealed>
passcode=<concealed>
eos_user_cert=<concealed>
eos_user_key=<concealed>
```

### Script

```bash
# Copy cert and password-less key
curl -o ./usercert.pem -u ${repo_user}:${repo_passwd} https://eospublichttp.cern.ch:8443/${eos_user_cert}

cat > passwd <<EOF
${proxy_passwd}
EOF

rm -fv userkey.pem

curl -o ./userkey_b.pem -u ${repo_user}:${repo_passwd} https://eospublichttp.cern.ch:8443/${eos_user_key}
openssl rsa -in "userkey_b.pem" -passin "file:passwd" -out "userkey.pem"

chmod 0400 ./userkey.pem
rm -fv passwd

# Set env
export X509_USER_CERT=~+/usercert.pem
export X509_USER_KEY=~+/userkey.pem
echo $X509_USER_CERT
echo $X509_USER_KEY

# Create proxy
voms-proxy-init --voms dteam:/dteam/Role=lcgadmin

#rm -rf /var/log/ftssuite/*
touch /var/log/ftssuite/ftstest_master.log

echo $test_password | kinit $test_user
aklog

export FTS3_HOST="fts3-pilot.cern.ch"
echo "SendEnv FTS3_HOST" >> ~/.ssh/config

export BUILD_ID=dontKillMe
cp masterSlave/config_dpm/config.py masterSlave/config.py # FTS3 DPM Stress Test
cp masterSlave/config_gridftp/config.py masterSlave/config.py # FTS3 GridFTP Stress Test
cp masterSlave/config_root/config.py masterSlave/config.py # FTS3 Root Stress Test
cp masterSlave/config_optimizer/config.py masterSlave/config.py # FTS3 Optimizer Stress Test
cp masterSlave/config_doppelganger/config.py masterSlave/config.py # FTS3 Doppelganger Stress Test

python testSubmitMaster.py
```
