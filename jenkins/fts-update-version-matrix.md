fts-update-version-matrix
=========================

Repository: https://gitlab.cern.ch/fts/build-utils.git

### Variables
```
doc_dir=/eos/workspace/f/fts/repo/www/versions
```

### Script

```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

./scripts/doc-extractor scripts/fts.json "${doc_dir}"
```
