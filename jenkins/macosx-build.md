macosx-build
============

Repository: https://gitlab.cern.ch/fts/build-utils.git  
Environment: MacOS X  
Output: MacOS packages

### Variables
```
homebrew_base=/Users/jenkins/homebrew-repo/homebrew-base.tgz
homebrew_path=/Users/jenkins/Builds/homebrew/
```

### Script

```bash
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" gfal2
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" gfal2-python
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" gfal2-util
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" srm-ifce
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" cgsi-gsoap
./scripts/brewmock --base "${homebrew_base}" --dir "${homebrew_path}" --clean --tap "cern-fts/dmc" cern-fts/dmc/davix --build-from-source
```
