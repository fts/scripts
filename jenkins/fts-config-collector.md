fts3-test-suite-pilot
=====================

Repository: https://gitlab.cern.ch/fts/fts-config-collector.git  

### Variables
```
login=<concealed>
passcode=<concealed>
eos_user_cert=<concealed>
eos_user_key=<concealed>
```

### Script

```bash
# Copy cert and password-less key
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

cat > passwd <<EOF
${proxy_passwd}
EOF

rm -fv userkey.pem

cp "${eos_user_cert}" ./usercert.pem
openssl rsa -in "${eos_user_key}" -passin "file:passwd" -out "userkey.pem"

chmod 0400 ./userkey.pem
rm -fv passwd

# Set env
export X509_USER_CERT=~+/usercert.pem
export X509_USER_KEY=~+/userkey.pem
echo $X509_USER_CERT
echo $X509_USER_KEY

# Create proxy
voms-proxy-init --voms dteam:/dteam/Role=lcgadmin --order /dteam/Role=lcgadmin

# Run test
if [ ! -d "go" ]; then
    mkdir -p $HOME/go/src
 	mkdir -p $HOME/go/bin
fi

export GOPATH=$HOME/go
export PATH=$HOME/go/bin:$PATH
export proxy=`voms-proxy-info -path`

cd $HOME/go/src

go get gitlab.cern.ch/fts/fts-config-collector/...

cd $HOME/go/src/gitlab.cern.ch/fts/fts-config-collector/main

go build main.go
go install

cd $HOME/go/bin

./main --login ${login} --passwd ${passcode} --cert ${X509_USER_CERT} --key ${X509_USER_KEY} --proxy ${proxy} 
```
