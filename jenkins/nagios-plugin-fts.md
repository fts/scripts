nagios-plugin-fts
=================

Repository: https://gitlab.cern.ch/fts/nagios-plugins-fts.git
Output: RPMs

### Variables
```
repo_base=/eos/workspace/f/fts/repo/www/repos/testing
repo_base=/eos/workspace/f/fts/repo/www/repos/rc
```

### Script

```bash
rm -rf build-utils/
git clone https://gitlab.cern.ch/fts/build-utils.git
```
```bash
cd packaging/
RPMBUILD=`mktemp -d`
make mock RPMBUILD="${RPMBUILD}" MOCK_FLAGS="--verbose --configdir=../build-utils/mock-config --resultdir=." MOCK_CHROOT=$chroot RELEASE=r`date +%y%m%d%H%M`
rm -rfv "${RPMBUILD}"
```
```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
aklog

./build-utils/scripts/copy2repo --repo-base "${repo_base}" --mock-config "./build-utils/mock-config/${chroot}.cfg" packaging/*.rpm
```
