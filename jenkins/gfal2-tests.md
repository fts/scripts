gfal2-tests
===========

Repository: https://gitlab.cern.ch/dmc/gfal2.git  
Environment: Docker  
Output: `gfal2-tests/*/Test.xml` 

### Script

```bash
BUILDDIR=`pwd`

rm -rf gfal2-tests/
mkdir gfal2-tests/

cd test/
docker build --pull --rm=true -t gfal2-tests .
docker run --rm=true -u `id -u`:`id -g` -v "/home/jenkins/.ssh:/.ssh" -v "/home/jenkins/.globus:/.globus" -v "${BUILDDIR}/gfal2-tests:/usr/share/gfal2/tests/Testing" -v "/etc/passwd:/etc/passwd" gfal2-tests "${proxy_passwd}" || true
```
