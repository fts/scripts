qa-gfal2
========

Repository: https://gitlab.cern.ch/dmc/gfal2.git  
Environment: Docker  

### Script

```bash
BUILDDIR=`pwd`

cd qa/
docker build --pull --no-cache=true --rm=true -t gfal2-cov .
docker run --rm=true -v "$BUILDDIR:/gfal2" -v "$HOME/.ssh:/.ssh" -v "$HOME/.globus:/.globus" -v "/etc/passwd:/etc/passwd" -u `id -u`:`id -g` gfal2-cov "${proxy_passwd}"
```
```bash
docker rmi gfal2-cov
```
