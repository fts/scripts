sts-server
==========

Repository: https://gitlab.cern.ch/sts/sts-server.git  
Environment: Maven build  
Root POM: `pom.xml`  
Output: `target/*.war`
