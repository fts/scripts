fts3-test-suite
===============

Repository: https://gitlab.cern.ch/fts/fts3TestSuite.git  

### Variables
```
eos_user_cert=<concealed>
eos_user_key=<concealed>
test_user=<concealed>
test_password=<concealed>
```

### Script

```bash
# Copy cert and password-less key
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

cat > passwd <<EOF
${proxy_passwd}
EOF

rm -fv userkey.pem

cp "${eos_user_cert}" ./usercert.pem
openssl rsa -in "${eos_user_key}" -passin "file:passwd" -out "userkey.pem"

chmod 0400 ./userkey.pem
rm -fv passwd

# Set env
export X509_USER_CERT=~+/usercert.pem
export X509_USER_KEY=~+/userkey.pem
echo $X509_USER_CERT
echo $X509_USER_KEY

# Create proxy
voms-proxy-init --voms dteam:/dteam/Role=lcgadmin

#rm -rf /var/log/ftssuite/*
touch /var/log/ftssuite/ftstest_master.log

echo $test_password | kinit $test_user
aklog

export BUILD_ID=dontKillMe
#export BUILD_ID=allow_to_run_as_daemon
#export LANG=ja_JP.UTF-8 

cp masterSlave/config_dbScalabilityTest/config.py masterSlave/config.py # FTS3 Stress Test
cp masterSlave/config_optimizer/config.py masterSlave/config.py # FTS3 Optimizer Stress Test
cp masterSlave/config_automatic_session_reuse/config.py masterSlave/config.py # FTS3 Automatic Session Reuse Test
cp masterSlave/config_dpm/config.py masterSlave/config.py # FTS3 DPM (XRoot?) (Checksum?) Stress Test

python testSubmitMaster.py
```
