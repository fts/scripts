debian-build
============

### Distribution matrix

```
distribution=debian/testing
distribution=debian/jessie
distribution=debian/stretch
distribution=ubuntu/artful
distribution=ubuntu/bionic
```

### Build base image

```bash
dist=$(echo $distribution | cut -d '/' -f 1)
name=$(echo $distribution | cut -d '/' -f 2)

sudo pbuilder create --distribution ${name} --mirror ftp://mirror.switch.ch/mirror/${dist}/ --othermirror "deb [trusted=yes] http://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/apt/${dist}/${name}/ dmc/" --basetgz /var/cache/pbuilder/base-${dist}-${name}.tgz
```

### Build packages

Repository: https://gitlab.cern.ch/dmc/gfal2.git  
Repository: https://gitlab.cern.ch/dmc/gfal2-python.git  
Repository: https://gitlab.cern.ch/dmc/gfal2-util.git  
Repository: https://gitlab.cern.ch/dmc/davix.git  
Repository: https://gitlab.cern.ch/dmc/srm-ifce.git  
Repository: https://gitlab.cern.ch/dmc/cgsi-gsoap.git  
Output: DEB packages

#### Variables
```
repo_base=/afs/cern.ch/project/gd/www/dms/dmc/repos/apt
```

#### Script
```bash
rm -rf build-utils/
git clone https://gitlab.cern.ch/fts/build-utils.git
```
```bash
dist=$(echo $distribution | cut -d '/' -f 1)
name=$(echo $distribution | cut -d '/' -f 2)

BASETGZ="${repo_base}/base-${dist}-${name}.tgz"
MIRROR="https://mirror.switch.ch/ftp/mirror/${dist}/"
OTHERMIRROR="deb [trusted=yes] https://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/apt/${distribution}/ dmc/"
if [ "${dist}" = "ubuntu" ]; then
    OTHERMIRROR="$OTHERMIRROR|deb https://mirror.switch.ch/ftp/mirror/${dist} ${name} universe"
fi

sudo rsync -v "${repo_base}/base-${dist}-${name}.tgz" "${BASETGZ}"

TEMPDIR=`mktemp -d`
cd packaging/
sudo make deb \
    PBUILDER_FLAGS="--buildresult . --distribution '${name}' --basetgz '${BASETGZ}' --mirror '${MIRROR}' --othermirror '${OTHERMIRROR}'" \
    RELEASE=r`date +%y%m%d%H%M`\
    PBUILDER_TMP="${TEMPDIR}"
ls *.deb
sudo rm -rf "${TEMPDIR}"
```
```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
aklog

./build-utils/scripts/copy2apt --repo-base "${repo_base}/${distribution}" packaging/*.deb
```

### Update repository

#### Variables
```
repo_base=/afs/cern.ch/project/gd/www/dms/dmc/repos/apt
```

#### Script
```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
aklog

mkdir -p "${repo_base}/${distribution}/dmc"
cd "${repo_base}/${distribution}"

apt-ftparchive packages "." > "dmc/Packages"
gzip -c "dmc/Packages" > "dmc/Packages.gz"

cd dmc/
apt-ftparchive release "." | cat Release.in - > Release
```
```bash
dist=$(echo $distribution | cut -d '/' -f 1)
name=$(echo $distribution | cut -d '/' -f 2)

BASETGZ="/var/cache/pbuilder/base-${dist}-${name}.tgz"
MIRROR="https://mirror.switch.ch/ftp/mirror/${dist}/"
OTHERMIRROR="deb [trusted=yes] https://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/apt/${distribution}/ dmc/"
if [ "${dist}" = "ubuntu" ]; then
    OTHERMIRROR="$OTHERMIRROR|deb https://mirror.switch.ch/ftp/mirror/${dist} ${name} universe"
fi

sudo pbuilder update --override-config --distribution "${name}" \
  --mirror "${MIRROR}" \
  --othermirror "${OTHERMIRROR}" \
  --basetgz "${BASETGZ}"

cp -vf "${BASETGZ}" "${repo_base}/base-${dist}-${name}.tgz"
```
