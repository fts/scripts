sonar-qa-fts3
=============

Repository: https://gitlab.cern.ch/fts/ftsmon_aggregations.git  

### Variables
```
test_user=<concealed>
test_password=<concealed>
user_es=<concealed>
password_es=<concealed>
user_monit=<concealed>
password_monit=<concealed>
```

### Script

```bash
# Set env
export USER_ES=${user_es}
export PASSWORD_ES=${password_es}
export USER_MONIT=${user_monit}
export PASSWORD_MONIT=${password_monit}

echo ${test_password} | kinit ${test_user}
aklog

python fts_monit_5years.py
```
```bash
# On build failure
message=":warning: [${JOB_NAME}](${BUILD_URL}) failed or unstable"
curl -i -X POST -d "payload={\"text\": \"${message}\", \"username\":\"fts.alarms\"}" ${MATTERMOST_HOOK}
```
