dmc-update-version-matrix
=========================

Repository: https://gitlab.cern.ch/fts/build-utils.git

### Variables
```
doc_dir=/eos/project/d/dmc/www/versions
```

### Script

```bash
kinit ${repo_user}@CERN.CH <<EOF
${repo_passwd}
EOF
eosfusebind

./scripts/doc-extractor scripts/dmc.json "${doc_dir}"
```
