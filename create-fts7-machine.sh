#!/bin/bash

export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_PROJECT_NAME="IT FTS"
export OS_AUTH_URL=https://keystone.cern.ch/v3
export OS_USERNAME=$USER



openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server not working"
  exit
fi

export ENVIRO="qa"
export IMAGE='CC7 - x86_64 [2018-05-16]'

KEYPAIR="fts3ops"

NUMBER=$1

STRING="fts"$NUMBER".cern.ch"
    ai-bs \
        --foreman-hostgroup 'fts/pilot/live' \
         --foreman-environment "${ENVIRO}" \
         --landb-mainuser 'FTS-3RD' \
         --landb-responsible 'FTS-3RD' \
         --nova-flavor "m2.large" \
         --nova-sshkey "${KEYPAIR}" \
         --landb-ipv6ready \
         --nova-image "${IMAGE}" \
     $STRING
  


