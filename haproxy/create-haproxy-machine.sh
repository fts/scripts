#!/bin/bash

export OS_IDENTITY_API_VERSION=3
export OS_PROJECT_DOMAIN_ID=default
export OS_PROJECT_NAME="IT FTS"
export OS_AUTH_URL=https://keystone.cern.ch/krb/v3
export OS_USERNAME=$USER



openstack server list  > /dev/null 2>&1
if [ $? != "0" ] ; then
  echo "openstack server not working"
  exit
fi

export ENVIRO="haproxy"
export IMAGE='CC7 Base - x86_64 [2015-02-10]'

KEYPAIR="fts3ops"

NUMBER=$1

STRING="fts-haproxy-devel${NUMBER}.cern.ch"
    ai-bs-vm \
        --foreman-hostgroup 'fts/devel/mysql/live' \
         --foreman-environment "${ENVIRO}" \
         --landb-mainuser 'FTS-3RD' \
         --landb-responsible 'FTS-3RD' \
         --nova-flavor "m1.large" \
         --nova-sshkey "${KEYPAIR}" \
         --nova-parameter "landb-ipv6ready=true" \
         --nova-image "${IMAGE}" \
     $STRING
  


