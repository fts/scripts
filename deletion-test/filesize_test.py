#!/bin/python3

from statistics import mean, median
import gfal2
import optparse
import sys
import pickle
import errno
import time

results = []
context = gfal2.creat_context()
params = context.transfer_parameters()
params.timeout = 500
params.overwrite = True

def run_test(source, dest, n_files):
    for i in range(0,n_files):
        try:
            r = context.filecopy(params, source, dest)
            #print("%s\tCOPIED" % dest)
        except:
            print("Copy failed: %s" % str(e))
        time.sleep(1)
        try:
            start = time.time()
            context.unlink(dest)
            end = time.time()
            #print("%s\tDELETED" % dest)
            results.append(end-start)
        except gfal2.GError:
            e = sys.exc_info()[1]
            if e.code == errno.ENOENT:
                print("%s\tMISSING" % dest)
            else:
                print("%s\tFAILED" % dest)
        time.sleep(2)

def get_metrics():
    with open('deletion.pkl', 'wb') as f:
        pickle.dump(results, f)
    try:
        avg = mean(results)
        results.sort()
        med = median(results)
        max_time = max(results)
        print("MAX TIME TO DELETE= %f" % max_time)
        print("AVERAGE TIME TO DELETE= %f" % avg)
        print("MEDIAN OF TIME TO DELETE= %f" % med)
    except:
        print("Dividing by zero")

def main():
    # Parse arguments
    parser = optparse.OptionParser()
    parser.add_option('--source', dest = 'source', type=str, default=None, help = 'Source file')
    parser.add_option('--dest', dest = 'dest', type=str, default=None, help = 'Destination file')
    parser.add_option('-f', '--files', dest = 'n_files', type=int, default=10)

    (options, args) = parser.parse_args()

    if options.source == None or options.dest == None:
        parser.error("Need a source and a destination")
        raise

    source = options.source
    dest = options.dest
    n_files = options.n_files

    start = time.time()
    run_test(source, dest, n_files)
    end = time.time()
    total = end - start
    print("TEST TOTAL TIME %f" % total)

    # Compute metrics
    get_metrics()

if __name__ == '__main__':
    main()
