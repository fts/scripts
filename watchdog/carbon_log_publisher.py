#!/usr/bin/env python

import sys
import json
import pickle
import struct
import socket
import argparse
from datetime import datetime


class CarbonLogPublisher:

  def __init__(self, mode, carbon_tag, carbon_host, carbon_port):
    self.mode = mode
    self.carbon_tag = carbon_tag
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def send_to_carbon(self, entries, timestamp):
    def sanitize_key(key):
      if self.carbon_host == "filer-carbon.cern.ch":
        key = key.replace("://", "__")
      return key.replace('.', '_')

    metrics = []
    for key, value in entries.iteritems():
      key = sanitize_key(key)
      metrics.append(("{}.{}".format(self.carbon_tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Carbon Log Publisher", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("log_file", type=str, help="Log file to export")
  parser.add_argument("-m", "--mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)
  args = parser.parse_args()

  if args.carbon_tag is None:
    print("Must set --carbon-tag value!")
    sys.exit(1)

  publisher = CarbonLogPublisher(args.mode, args.carbon_tag,
                                 args.carbon_host, args.carbon_port)

  with open(args.log_file, "r") as f:
    for line in f:
      data = line.split(" {")
      data[1] = "{" + data[1]
      entries = json.loads(data[1])
      timestamp = datetime.strptime(data[0], "%Y-%m-%d %H:%M:%S").strftime("%s")
      publisher.send_to_carbon(entries, timestamp)
