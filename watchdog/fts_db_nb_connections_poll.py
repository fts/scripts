#!/usr/bin/env python3

import sys
import json
import pickle
import struct
import socket
import argparse
import subprocess
import pymysql
from datetime import datetime, timedelta

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser


class FTSDatabaseNbConnectionsPoller:
  QUERY_NB_CONNECTIONS_PER_USER = """
    SELECT user, COUNT(*) FROM INFORMATION_SCHEMA.PROCESSLIST
    WHERE user LIKE '%fts%' AND DB LIKE '%fts%'
    GROUP BY user;
  """
  QUERY_CONNECTION_LIMITS = """
    SHOW VARIABLES LIKE 'max%_connections';
  """

  def __init__(self, fts_instance, database_config, database_type,
               mailing_list_file, alarm_limit, alarm_interval, alarm_timestamp_file,
               carbon_tag, carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.database_type = database_type
    self.alarm_limit = alarm_limit
    self.alarm_interval = alarm_interval
    self.alarm_timestamp_file = alarm_timestamp_file
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port
    self.connection = self._db_connection()
    self.mailing_list = self._read_mailing_list(mailing_list_file)

  @staticmethod
  def _log(message):
    if args.debug:
      print(message)

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  @staticmethod
  def _read_mailing_list(filename):
    try:
      with open(filename, "r") as f:
        return f.read().strip()
    except Exception:
      FTSDatabaseNbConnectionsPoller._log("Failed to read mailing list from file: {}".format(filename))
    return None

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    section = "database_admin_{}".format(self.database_type)
    return {
      'host': config.get(section, 'host'),
      'port': config.getint(section, 'port'),
      'name': config.get(section, 'name'),
      'user': config.get(section, 'user'),
      'password': config.get(section, 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _send_to_carbon(self, tag, entries, timestamp):
    metrics = []
    for key, value in entries.items():
      metrics.append(("{}.{}".format(tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def fetch_nb_connections(self):
    cursor = self.connection.cursor()
    timestamp = int(datetime.now().strftime("%s"))

    cursor.execute(self.QUERY_NB_CONNECTIONS_PER_USER)
    records = cursor.fetchall()
    nb_connections = {user: count for (user, count) in records}

    cursor.execute(self.QUERY_CONNECTION_LIMITS)
    records = cursor.fetchall()
    for (setting, limit) in records:
      nb_connections[setting] = limit

    self._log_as_json(args.log_file, nb_connections, timestamp)
    self._send_to_carbon("{}.{}.{}".format(self.carbon_tag, self.fts_instance, self.database_type),
                         nb_connections, timestamp)
    if self.mailing_list:
      self.should_trigger_alarm(nb_connections)

  def should_trigger_alarm(self, nb_connections):
    for user, count in nb_connections.items():
      if 'fts' in user and count >= self.alarm_limit and self.allowed_to_send_mail():
        self._write_alarm_timestamp()
        self.send_mail(count)

  def allowed_to_send_mail(self):
    """
    True when the last alert took place more than 10 minutes ago.
    A temporary file is kept with the last sent timestamp.
    """
    try:
      with open(self.alarm_timestamp_file, "r") as f:
        last_datetime = datetime.strptime(f.read().strip(), "%Y-%m-%d %H:%M:%S.%f")
        if datetime.now() - timedelta(minutes=self.alarm_interval) >= last_datetime:
          return True
        self._log("Not enough time has passed since {}".format(last_datetime))
    except (ValueError, IOError):
      self._log("File does not exist or failed to parse last datetime string (e.g.: empty)")
      return True
    except Exception:
      self._log("Unknown error occurred!")
    return False

  def _write_alarm_timestamp(self):
    with open(self.alarm_timestamp_file, "w") as f:
      f.write("{}\n".format(datetime.now()))

  def send_mail(self, connections):
    sendmail = subprocess.Popen(['/usr/sbin/sendmail', self.mailing_list],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                text=True) # To make stdin/stdout work with strings instead of bytes

    subject = "{}-{}".format(self.fts_instance.upper(), self.database_type.upper())
    sendmail.stdin.write('To: ' + self.mailing_list + '\n')
    sendmail.stdin.write('From: noreply@cern.ch\n')
    sendmail.stdin.write('Subject: [FTS ALARM] {} - Too many database connections\n'.format(subject))
    sendmail.stdin.write('{} has {} connections which is above {}\n'.format(subject, connections, self.alarm_limit))
    sendmail.stdin.close()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Number Of Connections Polling", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  group = parser.add_mutually_exclusive_group()
  group.add_argument("--main", action="store_const", dest="database_type", const="main", default="main",
                     help="Flag that we are polling the database main replica")
  group.add_argument("--replica", action="store_const", dest="database_type", const="replica",
                     help="Flag that we are polling the database read-only replica")
  parser.add_argument("-M", "--mailing-list-file", type=str, help="File containing comma-separated list of e-mail addresses")
  parser.add_argument("-L", "--alarm-limit", type=int, help="User connections limit to trigger a mail alarm", default=1000)
  parser.add_argument("-I", "--alarm-interval", type=int, help="Wait time between mail alarms (minutes)", default=30)
  parser.add_argument("-R", "--alarm-timestamp-file", type=str, help="File location for timestamp of last alarm",
                      default="/tmp/fts-watchdog/db_nb_connections.alarm_timestamp")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_nb_connections.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.nb_connections")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseNbConnectionsPoller(args.instance, args.config, args.database_type,
                                          args.mailing_list_file, args.alarm_limit,
                                          args.alarm_interval, args.alarm_timestamp_file,
                                          args.carbon_tag, args.carbon_mode,
                                          args.carbon_host, args.carbon_port)
  poller.fetch_nb_connections()
