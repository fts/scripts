#!/usr/bin/env python3

import sys
import json
import pickle
import struct
import socket
import argparse
import pymysql
from datetime import datetime

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser


class FTSDatabaseStorageSaturationPoller:
  QUERY_SE_PAIRS = """
    SELECT source_se, dest_se, file_state, COUNT(*) FROM t_file 
    WHERE file_state IN ('SUBMITTED', 'ACTIVE')
    GROUP BY source_se, dest_se, file_state;
  """
  QUERY_SE_LIMITS = """
    SELECT storage, inbound_max_active, outbound_max_active FROM t_se;
  """
  QUERY_LINK_LIMITS = """
    SELECT source_se, dest_se, min_active, max_active FROM t_link_config;
  """
  QUERY_SE_ACTIVE_SRC = """
    SELECT source_se, COUNT(*) FROM t_file
    WHERE file_state = "ACTIVE"
    GROUP BY source_se;
  """
  QUERY_SE_ACTIVE_DST = """
    SELECT dest_se, COUNT(*) FROM t_file
    WHERE file_state = "ACTIVE"
    GROUP BY dest_se;
  """

  def __init__(self, fts_instance, database_config, carbon_tag,
               carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port
    self.storage_limits = {}
    self.link_limits = {}    
    self.storage_saturations = {}
    self.timestamp = None

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  @staticmethod
  def _print_table_to_stdout(storage_saturations, duration):
    print("{:<50} {:<50} {:>10} {:>10} {:>15} {:>15} {:>18} {:>17} {:>18} {:>17}".format("Source", "Destination", "Queue", "Active", "Min_Limit", "Max_Limit", "Outbound Active", "Outbound Limit", "Inbound Active", "Inbound Limit"))
    print("-" * ((50 + 50 + 10 + 10 + 15 + 15 + 18 + 17 + 18 + 17) + 10))
    for tuple_entry in sorted(storage_saturations.items(), key=lambda x: x[1]["queue"], reverse=True):
      saturation = tuple_entry[1]
      print("{:<50} {:<50} {:>10} {:>10} {:>15} {:>15} {:>18} {:>17} {:>18} {:>17}".format(
        saturation["source"],
        saturation["destination"],
        saturation["queue"],
        saturation["active"],
        saturation["min_limit"],
        saturation["max_limit"],
        saturation["outbound_active"],
        saturation["outbound_limit"],
        saturation["inbound_active"],
        saturation["inbound_limit"]
        ))
    sum_queue = sum(item["queue"] for item in storage_saturations.values())
    sum_active = sum(item["active"] for item in storage_saturations.values())
    print("-" * ((50 + 50 + 10 + 10 + 15 + 15 + 18 + 17 + 18 + 17) + 10))
    print("{:<50} {:<50} {:>10} {:>10}".format("Total Queue", "Total Active", sum_queue, sum_active))
    print("\nQuery duration: {:.3f}s".format(duration))

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    return {
      'host': config.get('database', 'host'),
      'port': config.getint('database', 'port'),
      'name': config.get('database', 'name'),
      'user': config.get('database', 'user'),
      'password': config.get('database', 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _build_metrics_se(self, tag, timestamp):
    """
    fts.db.sat.<instance>.se.<storage>.limit.inbound 300
    fts.db.sat.<instance>.se.<storage>.limit.outbound 300

    fts.db.sat.<instance>.se.<storage>.active.inbound 30
    fts.db.sat.<instance>.se.<storage>.active.outbound 30
    """
    metrics_se = []

    for storage, limits in self.storage_limits.items():
      storage = storage.replace(".", "_").replace("://", "__")
      metrics_se.append(("{}.se.{}.limit.inbound".format(tag, storage), (timestamp, limits["inbound"])))
      metrics_se.append(("{}.se.{}.limit.outbound".format(tag, storage), (timestamp, limits["outbound"])))

    for storage, actives in self.storage_actives.items():
      storage = storage.replace(".", "_").replace("://", "__")
      metrics_se.append(("{}.se.{}.active.inbound".format(tag, storage), (timestamp, actives["inbound"])))
      metrics_se.append(("{}.se.{}.active.outbound".format(tag, storage), (timestamp, actives["outbound"])))

    return metrics_se

  def _build_metrics_link(self, tag, timestamp):
    """
    fts.db.sat.<instance>.link.<source>.<destination>.queue 500
    fts.db.sat.<instance>.link.<source>.<destination>.active 50
    fts.db.sat.<instance>.link.<source>.<destination>.limit_min 2
    fts.db.sat.<instance>.link.<source>.<destination>.limit_max 100
    """
    metrics_link = []

    for _, link in self.storage_saturations.items():
      source = link["source"].replace(".", "_").replace("://", "__")
      destination = link["destination"].replace(".", "_").replace("://", "__")
      metrics_link.append(("{}.link.{}.{}.queue".format(tag, source, destination), (timestamp, link["queue"])))
      metrics_link.append(("{}.link.{}.{}.active".format(tag, source, destination), (timestamp, link["active"])))
      metrics_link.append(("{}.link.{}.{}.limit_min".format(tag, source, destination), (timestamp, link["min_limit"])))
      metrics_link.append(("{}.link.{}.{}.limit_max".format(tag, source, destination), (timestamp, link["max_limit"])))

    return metrics_link

  def _build_metrics_duration(self, tag, timestamp):
    return [("{}.duration".format(tag), (timestamp, self.elapsed.total_seconds()))]

  def _send_to_carbon(self, metrics, timestamp):
    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def _fetch_storage_limits(self, cursor):
    cursor.execute(self.QUERY_SE_LIMITS)
    records = cursor.fetchall()
    storage_limits = {storage: {"inbound": inbound, "outbound": outbound} for (storage, inbound, outbound) in records}

    if "*" not in storage_limits:
      sys.exit("Did not find default '*' storage configuration. Aborting!")

    for storage, limits in storage_limits.items():
      if limits["inbound"] is None:
        storage_limits[storage]["inbound"] = storage_limits["*"]["inbound"]
      if limits["outbound"] is None:
        storage_limits[storage]["outbound"] = storage_limits["*"]["outbound"]
    
    return storage_limits

  def _fetch_link_limits(self, cursor):
    cursor.execute(self.QUERY_LINK_LIMITS)
    records = cursor.fetchall()
    link_limits = {"{}_{}".format(source, destination): {"min_active": min_active, "max_active": max_active} for (source, destination, min_active, max_active) in records}

    if "*_*" not in link_limits:
      sys.exit("Did not find default '* -> *' link configuration. Aborting!")

    return link_limits

  def _fetch_storage_actives(self, cursor):
    cursor.execute(self.QUERY_SE_ACTIVE_SRC)
    src_records = cursor.fetchall()
    cursor.execute(self.QUERY_SE_ACTIVE_DST)
    dst_records = cursor.fetchall()

    storage_actives = {storage: {"inbound": 0, "outbound": count} for (storage, count) in src_records}
    for (storage, count) in dst_records:
      if storage in storage_actives:
        storage_actives[storage]["inbound"] = count
      else:
        storage_actives[storage] = {"inbound": count, "outbound": 0}

    return storage_actives

  def _prepare_storage_saturations(self, cursor):
    cursor.execute(self.QUERY_SE_PAIRS)
    records = cursor.fetchall()

    # 1st iteration: register all (source, destination) pairs
    storage_saturations = {"{}_{}".format(source, destination): {"source": source, "destination": destination, "queue": 0, "active": 0} for (source, destination, _, _) in records}

    # 2nd iteration: register SUBMITTED / ACTIVE count as queue / active 
    for (source, destination, file_state, count) in records:
      pair = "{}_{}".format(source, destination)
      if file_state == "SUBMITTED":
        storage_saturations[pair]["queue"] = count
      elif file_state == "ACTIVE":
        storage_saturations[pair]["active"] = count
    
    # 3rd iteration: add storage and link limits to (source, destination) pairs
    for pair, saturation in storage_saturations.items():
        source = saturation["source"]
        destination = saturation["destination"]
        
        source_names = [source, "*"]
        for source_name in source_names:
          if source_name in self.storage_limits:
            storage_saturations[pair]["outbound_limit"] = self.storage_limits[source_name]["outbound"]
            break

        destination_names = [destination, "*"]
        for destination_name in destination_names:
          if destination_name in self.storage_limits:
            storage_saturations[pair]["inbound_limit"] = self.storage_limits[destination_name]["inbound"]
            break

        link_names = [pair, "*_{}".format(destination), "{}_*".format(source), "*_*"]
        for link_name in link_names:
          if link_name in self.link_limits:
            storage_saturations[pair]["min_limit"] = self.link_limits[link_name]["min_active"]
            storage_saturations[pair]["max_limit"] = self.link_limits[link_name]["max_active"]
            break

        storage_saturations[pair]["outbound_active"] = self.storage_actives[source]["outbound"] if source in self.storage_actives else 0
        storage_saturations[pair]["inbound_active"] = self.storage_actives[destination]["inbound"] if destination in self.storage_actives else 0

    return storage_saturations

  def fetch_se_saturation(self):
    connection = self._db_connection()
    cursor = connection.cursor()

    timestamp = datetime.now()
    self.storage_limits = self._fetch_storage_limits(cursor)
    self.link_limits = self._fetch_link_limits(cursor)
    self.storage_actives = self._fetch_storage_actives(cursor)
    self.storage_saturations = self._prepare_storage_saturations(cursor)
    self.elapsed = datetime.now() - timestamp
    timestamp = int(timestamp.strftime("%s"))

    if args.print_table:
      self._print_table_to_stdout(self.storage_saturations, self.elapsed.total_seconds())
      return

    metrics = []
    tag = "{}.{}".format(self.carbon_tag, self.fts_instance)
    metrics.extend(self._build_metrics_se(tag, timestamp))
    metrics.extend(self._build_metrics_link(tag, timestamp))
    metrics.extend(self._build_metrics_duration(tag, timestamp))

    # Add "duration" to JSON logging
    self.storage_saturations["duration"] = self.elapsed.total_seconds()

    self._log_as_json(args.log_file, self.storage_saturations, timestamp)
    self._send_to_carbon(metrics, timestamp)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Storage Saturation Polling", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-P", "--print", help="Print table format to stdout (doesn't send anything to the Carbon host)",
                      action="store_true", default=False, dest="print_table")
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_se_saturation.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.sat")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseStorageSaturationPoller(
    fts_instance=args.instance, 
    database_config=args.config, 
    carbon_tag=args.carbon_tag,
    carbon_mode=args.carbon_mode, 
    carbon_host=args.carbon_host, 
    carbon_port=args.carbon_port)
  poller.fetch_se_saturation()
