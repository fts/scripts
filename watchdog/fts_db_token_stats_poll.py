#!/usr/bin/env python3

import sys
import json
import pickle
import struct
import socket
import argparse
import pymysql
from datetime import datetime

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser


class FTSDatabaseTokenStatsPoller:
  TOKENS_STATS_ALL_QUERY          = "SELECT COUNT(*) FROM t_token;"
  TOKENS_STATS_WITH_REFRESH_QUERY = "SELECT COUNT(*) FROM t_token WHERE refresh_token IS NOT NULL;"
  TOKENS_STATS_RETIRED_QUERY      = "SELECT COUNT(*) FROM t_token WHERE retired = '1';"

  def __init__(self, fts_instance, database_config, carbon_tag,
               carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    return {
      'host': config.get('database', 'host'),
      'port': config.getint('database', 'port'),
      'name': config.get('database', 'name'),
      'user': config.get('database', 'user'),
      'password': config.get('database', 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _send_to_carbon(self, tag, entries, timestamp):
    metrics = []
    for key, value in entries.items():
      metrics.append(("{}.{}".format(tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def fetch_token_stats(self):
    connection = self._db_connection()
    cursor = connection.cursor()

    timestamp = datetime.now()
    cursor.execute(self.TOKENS_STATS_ALL_QUERY)
    token_records_all = cursor.fetchall()[0][0]
    cursor.execute(self.TOKENS_STATS_WITH_REFRESH_QUERY)
    token_records_with_refresh = cursor.fetchall()[0][0]
    cursor.execute(self.TOKENS_STATS_RETIRED_QUERY)
    token_records_retired = cursor.fetchall()[0][0]
    elapsed = datetime.now() - timestamp

    token_stats = {
      "all": token_records_all,
      "with_refresh": token_records_with_refresh,
      "without_refresh": token_records_all - token_records_with_refresh,
      "retired": token_records_retired,
      "not_retired": token_records_all - token_records_retired,
      "duration": elapsed.total_seconds()
    }
    timestamp = int(timestamp.strftime("%s"))
    self._log_as_json(args.log_file, token_stats, timestamp)
    self._send_to_carbon("{}.{}".format(self.carbon_tag, self.fts_instance),
                         token_stats, timestamp)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Token Stats Polling", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_token_stats.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.tokens")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseTokenStatsPoller(args.instance, args.config, args.carbon_tag,
                                       args.carbon_mode, args.carbon_host, args.carbon_port)
  poller.fetch_token_stats()
