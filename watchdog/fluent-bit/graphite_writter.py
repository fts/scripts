from ctypes import ArgumentError
import json
import socket
import struct
import pickle
from contextlib import contextmanager

DEFAULT_PORT=2003
USE_PICKLE=True

# Manage connection to graphite

class GraphitePickleConn:

    def __init__(self, hostname, port, use_pickle):
        self._hostname = hostname
        self._port = port
        self._conn = None
        self._use_pickle = use_pickle

    def open(self):
        self._conn = socket.create_connection((self._hostname, self._port))

    def close(self):
        self._conn.close()

    def send(self, metric_list):

        if self._conn is None:
            raise RuntimeError("Connection not open")

        if self._use_pickle:
            payload = pickle.dumps(metric_list, protocol=2)
            header = struct.pack("!L", len(payload))
            self._conn.send(header)
            self._conn.send(payload)
        else:
            message = ''.join(["{} {} {}\n".format(metric_name, value, timestamp) for (metric_name, (timestamp, value)) in metric_list])
            self._conn.send(message.encode())


# Context manager for opening/closing connections to graphite

@contextmanager
def open_GraphitePickleConn(hostname, port, use_pickle):
    conn = GraphitePickleConn(hostname, port, use_pickle)
    try:
        conn.open()
        yield conn
    finally:
        conn.close()


# Construct list of metrics and send them to connection

class GraphiteMetricWritter:

    def __init__(self, conn):
        self._conn = conn

    @staticmethod
    def flatten_dict(input, prefix='', separator='.'):
        output = {}

        def _flatten(x, metric_prefix=''):
            if isinstance(x, dict):
                for key, val in x.items():
                    _flatten(val, metric_prefix + separator + key)
            elif isinstance(x, list):
                for idx, val in enumerate(x):
                    _flatten(val, metric_prefix + separator + idx)
            else:
                output[metric_prefix] = x

        _flatten(input, prefix)
        return output

    @staticmethod
    def flatten_json(input_json, prefix='', separator='.'):
        input_parsed = json.loads(input_json)
        return GraphiteMetricWritter.flatten_dict(input_parsed, prefix, separator)

    def _send(self, metric_list):
        self._conn.send(metric_list)

    def send_json(self, timestamp, input, prefix=''):
        flat_data = GraphiteMetricWritter.flatten_json(input, prefix)
        metrics = [(metric_name, (timestamp, value)) for metric_name, value in flat_data.items()]
        self._send(metrics)

    def send_dict(self, timestamp, input, prefix=''):
        flat_data = GraphiteMetricWritter.flatten_dict(input, prefix)
        metrics = [(metric_name, (timestamp, value)) for metric_name, value in flat_data.items()]
        self._send(metrics)


# Context manager for creating new connection and writting structured data

@contextmanager
def open_GraphiteMetricWritter(hostname, port, use_pickle):
    with open_GraphitePickleConn(hostname, port, use_pickle) as conn:
        try:
            writter = GraphiteMetricWritter(conn)
            yield writter
        finally:
            pass


if __name__ == "__main__":

    import sys
    import json
    import contextlib
    import argparse

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("carbon_host", type=str)
    arg_parser.add_argument("carbon_port", type=int)
    arg_parser.add_argument("--use-pickle", action="store_true")
    args = arg_parser.parse_args()

    with contextlib.redirect_stdout(sys.stderr):

        try:
            #print("[START] Sending data to metrictank")
            with open_GraphiteMetricWritter(args.carbon_host, port=args.carbon_port, use_pickle=args.use_pickle) as wg:
                for line in sys.stdin:

                    data = json.loads(line)
                    metric_data = data.pop("metric")
                    timestamp = data.pop("timestamp")

                    service_name = data.pop("service")
                    host_name = data.pop("hostname").replace(".", "_")
                    metric_namespace = data.pop("metric_namespace")
                    prefix = ".".join([service_name, host_name, metric_namespace])
                    wg.send_dict(timestamp, metric_data, prefix)

        finally:
            pass
            #print("[END] Sending data to metrictank")

