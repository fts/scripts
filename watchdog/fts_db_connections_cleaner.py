#!/usr/bin/env python3

import sys
import json
import pickle
import struct
import socket
import argparse
import pymysql
from datetime import datetime

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser


class FTSDatabaseStalledConnectionsCleaner:
  QUERY_STALLED_CONNECTIONS = """
  SELECT id FROM INFORMATION_SCHEMA.PROCESSLIST
  WHERE user LIKE '%fts3%' AND (
    (command = 'Sleep' AND time > 900) OR
    (info LIKE 'SELECT COUNT(FILE_STATE) as count, file_state, source_se, dest_se, vo_name%' AND time > 300)
  );
  """
  QUERY_CLEAN_CONNECTION_TEMPLATE = """
  KILL @ID;
  """

  def __init__(self, fts_instance, database_config, database_type,
               carbon_tag, carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.database_type = database_type
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port
    self.connection = self._db_connection()

  @staticmethod
  def _log(message):
    if args.debug:
      print(message.strip())

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    section = "database_admin_{}".format(self.database_type)
    return {
      'host': config.get(section, 'host'),
      'port': config.getint(section, 'port'),
      'name': config.get(section, 'name'),
      'user': config.get(section, 'user'),
      'password': config.get(section, 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _send_to_carbon(self, tag, entries, timestamp):
    metrics = []
    for key, value in entries.items():
      metrics.append(("{}.{}".format(tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if args.dry_run:
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def clean_connections(self):
    cursor = self.connection.cursor()
    timestamp = int(datetime.now().strftime("%s"))

    cursor.execute(self.QUERY_STALLED_CONNECTIONS)
    records = cursor.fetchall()
    connection_ids = [id[0] for id in records]
    connections_report = {"size": len(connection_ids)}

    for conn_id in connection_ids:
      query = self.QUERY_CLEAN_CONNECTION_TEMPLATE.replace("@ID", str(conn_id))
      self._log(query)
      if not args.dry_run:
        cursor.execute(query)

    self._log_as_json(args.log_file, connections_report, timestamp)
    self._send_to_carbon("{}.{}.{}".format(self.carbon_tag, self.fts_instance, self.database_type),
                         connections_report, timestamp)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Stalled Connections Cleaner", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  group = parser.add_mutually_exclusive_group()
  group.add_argument("--main", action="store_const", dest="database_type", const="main", default="main",
                     help="Flag that we are polling the database main replica")
  group.add_argument("--replica", action="store_const", dest="database_type", const="replica",
                     help="Flag that we are polling the database read-only replica")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-r", "--dry-run", help="Find stalled connections but do not cancel them. Should be used together with '--debug' option.",
                      action="store_true", default=False)
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_connections_cleaner.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.connections_cleaner")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseStalledConnectionsCleaner(args.instance, args.config, args.database_type,
                                                args.carbon_tag, args.carbon_mode,
                                                args.carbon_host, args.carbon_port)
  poller.clean_connections()
