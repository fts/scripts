#!/usr/bin/env python3

import sys
import re
import json
import pickle
import struct
import socket
import argparse
import pymysql
from datetime import datetime

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser


class FTSDatabaseStagingRequestsPoller:
  QUERY_REQUESTS_PER_STORAGE = """
    SELECT source_se, COUNT(DISTINCT bringonline_token) FROM t_file 
    WHERE vo_name = ':vo' AND file_state = 'STARTED' AND bringonline_token IS NOT NULL 
    GROUP BY source_se;  
  """

  def __init__(self, fts_instance, database_config, fts3_config,
               carbon_tag, carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.fts3_config = fts3_config
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port
    self.connection = self._db_connection()

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    return {
      'host': config.get('database', 'host'),
      'port': config.getint('database', 'port'),
      'name': config.get('database', 'name'),
      'user': config.get('database', 'user'),
      'password': config.get('database', 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _parse_config_option(self, pattern, default):
    with open(self.fts3_config, 'r') as fts3config:
      for line in fts3config:
        match = re.search(pattern, line)
        if match:
          return match.group(1)
    return default

  def _fill_query_parameters(self, query):
    return query.replace(":vo", self.fts_instance)

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _send_to_carbon(self, tag, entries, timestamp):
    def sanitize_key(key):
      if self.carbon_host == "filer-carbon.cern.ch":
        key = key.replace("://", "__")
      return key.replace('.', '_')

    metrics = []
    for key, value in entries.items():
      key = sanitize_key(key)
      metrics.append(("{}.{}".format(tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def fetch_staging_requests(self):
    query = self._fill_query_parameters(self.QUERY_REQUESTS_PER_STORAGE)
    cursor = self.connection.cursor()

    timestamp = datetime.now()
    cursor.execute(query)
    elapsed = datetime.now() - timestamp

    records = cursor.fetchall()
    staging_requests = {source_se: count for (source_se, count) in records}
    staging_requests["duration"] = elapsed.total_seconds()
    staging_requests["limit"] = self._parse_config_option('^StagingConcurrentRequests[\s]*=[\s]*([0-9]+)', 1000)
    timestamp = int(timestamp.strftime("%s"))
    self._log_as_json(args.log_file, staging_requests, timestamp)
    self._send_to_carbon("{}.count.{}".format(self.carbon_tag, self.fts_instance),
                         staging_requests, timestamp)


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Concurrent Staging Requests Polling", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  parser.add_argument("-f", "--fts3-config", type=str, help="FTS3 config file",
                      default="/etc/fts3/fts3config")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_staging_requests.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.staging_requests")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseStagingRequestsPoller(args.instance, args.config, args.fts3_config,
                                            args.carbon_tag, args.carbon_mode,
                                            args.carbon_host, args.carbon_port)
  poller.fetch_staging_requests()
