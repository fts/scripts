#!/usr/bin/env python3

import sys
import json
import pickle
import struct
import socket
import subprocess
import argparse
import pymysql
from datetime import datetime, timedelta

try:
  from configparser import ConfigParser
except ImportError:
  from ConfigParser import ConfigParser

class FTSDatabaseSecondsBehindMainPoller:
  QUERY_MAX_SECONDS_BEHIND_MAIN_REPLICA = """
  SHOW REPLICA STATUS;
  """

  def __init__(self, fts_instance, database_config,
               mailing_list_file, alarm_limit, alarm_interval, alarm_timestamp_file,
               carbon_tag, carbon_mode, carbon_host, carbon_port):
    self.fts_instance = fts_instance
    self.database_config = database_config
    self.alarm_limit = alarm_limit
    self.alarm_interval = alarm_interval
    self.alarm_timestamp_file = alarm_timestamp_file
    self.carbon_tag = carbon_tag
    self.carbon_mode = carbon_mode
    self.carbon_host = carbon_host
    self.carbon_port = carbon_port
    self.connection = self._db_connection()
    self.mailing_list = self._read_mailing_list(mailing_list_file)

  @staticmethod
  def _log(message):
    if args.debug:
      print(message)

  @staticmethod
  def _print_to_stdout(metrics):
    for metric in metrics:
      print("{} {} {}".format(metric[0], metric[1][1], metric[1][0]))

  @staticmethod
  def _log_as_json(filename, entries, timestamp):
    with open(filename, "a+") as f:
      f.write("{} ".format(datetime.fromtimestamp(timestamp).strftime("%Y-%m-%d %H:%M:%S")))
      json.dump(entries, f)
      f.write("\n")

  @staticmethod
  def _read_mailing_list(filename):
    try:
      with open(filename, "r") as f:
        return f.read().strip()
    except Exception:
      FTSDatabaseSecondsBehindMainPoller._log("Failed to read mailing list from file: {}".format(filename))
    return None

  def _parse_db_conn_details(self):
    config = ConfigParser()
    config.read(self.database_config)
    return {
      'host': config.get('database_admin_replica', 'host'),
      'port': config.getint('database_admin_replica', 'port'),
      'name': config.get('database_admin_replica', 'name'),
      'user': config.get('database_admin_replica', 'user'),
      'password': config.get('database_admin_replica', 'password')
    }

  def _db_connection(self):
    db_conn_details = self._parse_db_conn_details()
    return pymysql.connect(
      host=db_conn_details['host'],
      port=db_conn_details['port'],
      database=db_conn_details['name'],
      user=db_conn_details['user'],
      password=db_conn_details['password']
    )

  def _pickle_send(self, metrics):
    payload = pickle.dumps(metrics, protocol=2)
    header = struct.pack("!L", len(payload))
    message = header + payload
    conn = socket.create_connection((self.carbon_host, self.carbon_port))
    conn.send(message)
    conn.close()

  def _socket_send(self, metrics):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((self.carbon_host, self.carbon_port))
    for metric in metrics:
      metric_data = "{} {} {}\n".format(metric[0], metric[1][1], metric[1][0])
      s.send(metric_data.encode())
    s.close()

  def _send_to_carbon(self, tag, entries, timestamp):
    metrics = []
    for key, value in entries.items():
      metrics.append(("{}.{}".format(tag, key), (timestamp, value)))

    if args.debug:
      self._print_to_stdout(metrics)
      return

    if self.carbon_mode == "pickle":
      self._pickle_send(metrics)
    else:
      self._socket_send(metrics)

  def fetch_seconds_behind_main(self):
    cursor = self.connection.cursor(pymysql.cursors.DictCursor)
    timestamp = int(datetime.now().strftime("%s"))

    cursor.execute(self.QUERY_MAX_SECONDS_BEHIND_MAIN_REPLICA)
    records = cursor.fetchone()
    try:
      seconds_behind_main = {"max": int(records["Seconds_Behind_Source"])}
    except Exception:
      seconds_behind_main = {"max": None}
    self._log_as_json(args.log_file, seconds_behind_main, timestamp)
    self._send_to_carbon("{}.{}".format(self.carbon_tag, self.fts_instance),
                         seconds_behind_main, timestamp)

    if self.mailing_list:
      self._should_trigger_alarm(seconds_behind_main["max"])

  def _should_trigger_alarm(self, seconds_behind_main):
    if (seconds_behind_main is None or seconds_behind_main > self.alarm_limit) and self._allowed_to_send_mail():
      self._write_alarm_timestamp()
      self._send_mail(seconds_behind_main)

  def _allowed_to_send_mail(self):
    """
    True when the last alert took place more than 10 minutes ago.
    A temporary file is kept with the last sent timestamp.
    """
    try:
      with open(self.alarm_timestamp_file, "r") as f:
        last_datetime = datetime.strptime(f.read().strip(), "%Y-%m-%d %H:%M:%S.%f")
        if datetime.now() - timedelta(minutes=self.alarm_interval) >= last_datetime:
          return True
        self._log("Not enough time has passed since {}".format(last_datetime))
    except (ValueError, IOError):
      self._log("File does not exist or failed to parse last datetime string (e.g.: empty)")
      return True
    except Exception as ex:
      self._log("Unexpected error occurred when checking if allowed to send e-mail: {}".format(ex))
    return False

  def _write_alarm_timestamp(self):
    with open(self.alarm_timestamp_file, "w") as f:
      f.write("{}\n".format(datetime.now()))

  def _send_mail(self, seconds_behind_main):
    sendmail = subprocess.Popen(['/usr/sbin/sendmail', self.mailing_list],
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                text=True) # To make stdin/stdout work with strings instead of bytes

    subject = self.fts_instance.upper()
    sendmail.stdin.write('To: ' + self.mailing_list + '\n')
    sendmail.stdin.write('From: noreply@cern.ch\n')
    if seconds_behind_main is None:
      sendmail.stdin.write('Subject: [FTS ALARM] {} - Database replica seconds behind main polling returned None value\n'.format(subject))
      sendmail.stdin.write('The {} replica seconds behind main polling returned None value\n'.format(subject))
    else:
      sendmail.stdin.write('Subject: [FTS ALARM] {} - Database replica lagging too long behind main database\n'.format(subject))
      sendmail.stdin.write('The {} replica is lagging {} seconds behind the main database which is longer than {} seconds\n'.format(subject, seconds_behind_main, self.alarm_limit))
    sendmail.stdin.close()


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="CERN FTS Database Seconds Behind Main Replica Polling", add_help=True,
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-i", "--instance", type=str, help="Name of the FTS instance")
  parser.add_argument("-c", "--config", type=str, help="Config file with the DB connection details. Expected format: .ini",
                      default="/etc/fts3web/fts3web.ini")
  parser.add_argument("-M", "--mailing-list-file", type=str, help="File containing comma-separated list of e-mail addresses")
  parser.add_argument("-L", "--alarm-limit", type=int, help="Seconds behind main limit to trigger a mail alarm", default=1000)
  parser.add_argument("-I", "--alarm-interval", type=int, help="Wait time between mail alarms (minutes)", default=30)
  parser.add_argument("-R", "--alarm-timestamp-file", type=str, help="File location for timestamp of last alarm",
                      default="/tmp/fts-watchdog/db_seconds_behind_main.alarm_timestamp")
  parser.add_argument("-d", "--debug", help="Send output to stdout instead of the Carbon host",
                      action="store_true", default=False)
  parser.add_argument("-l", "--log-file", type=str, help="Log file location for polling results",
                      default="/tmp/fts-watchdog/db_seconds_behind_main.log")
  parser.add_argument("-t", "--carbon-tag", type=str, help="MetricTank tag prefix",
                      default="test.fts.db.seconds_behind_main")
  parser.add_argument("-m", "--carbon-mode", type=str, help="Send data using Pickle or socket",
                      default="pickle")
  parser.add_argument("-H", "--carbon-host", type=str, help="Send data to this Carbon Server",
                      default="metrictank-carbon.cern.ch")
  parser.add_argument("-p", "--carbon-port", type=int, help="Port of the Carbon Server (pickle support needed)",
                      default=2004)

  args = parser.parse_args()

  if args.instance is None:
    parser.print_help()
    sys.exit(1)

  poller = FTSDatabaseSecondsBehindMainPoller(args.instance, args.config,
                                              args.mailing_list_file, args.alarm_limit,
                                              args.alarm_interval, args.alarm_timestamp_file,
                                              args.carbon_tag, args.carbon_mode, args.carbon_host, args.carbon_port)
  poller.fetch_seconds_behind_main()
