from test_case_base import *
import time


class TestXAttr(TestCaseBase):

    # Get user.status until file is on tape
    def test_user_status(self):
        sleep = 1
        key = "user.status"
        expected_status = ["NEARLINE", "ONLINE_AND_NEARLINE"]
        status = ""
        while status not in expected_status:
            status = self.handle.context.getxattr(self.url, key)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertIn(status, expected_status)

    # Make sure the server is running a version supported by gfal
    def test_api_version(self):
        key = "taperestapi.version"
        supported_version = "v1"
        status = self.handle.context.getxattr(self.url, key)
        self.assertEqual(status, supported_version)
