from test_case_base import *
import time


class TestArchive(TestCaseBase):

    # Poll a single file for archiving
    def test_single_archive(self):
        sleep = 1
        status = 0
        while status == 0:
            status = self.handle.context.archive_poll(self.url)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertEqual(status, 1)

    # Poll an non-existing file a archiving
    def test_single_archive_enoent(self):
        file = generate_random_url(self.root, "tape_rest_api_enoent")
        try:
            status = self.handle.context.archive_poll(file)
            self.assertEqual(status, -1)
        except Exception as e:
            self.assertEqual(e.code, errno.ENOMSG)

    # Poll a list of files for archiving
    def test_bulk_archive(self):
        self._upload_and_register_files(self.bulk_size)
        sleep = 1
        status = 0
        while status == 0:
            (status, errors) = self.handle.archive_poll_list(self.remote_files)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertEqual(status, 1)
        self.assertAllNone(errors)

    # Poll a list of files for archiving (some files in the request do not exist)
    def test_bulk_archive_enoent(self):
        files_enoent = [generate_random_url(self.root, "tape_rest_api_enoent") for _ in range(self.bulk_size)]
        self._upload_and_register_files(self.bulk_size)
        urls = [el for pair in zip(self.remote_files, files_enoent) for el in pair]

        sleep = 1
        status = 0
        while status == 0:
            (status, errors) = self.handle.archive_poll_list(urls)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertEqual(status, 2)
        self.assertAllNone(errors[0::2])
        self.assertAllEqual([error.code for error in errors[1::2]], errno.ENOMSG)

    # Poll a list of files for archiving (the list of files contains duplicate entries)
    def test_archive_duplicates(self):
        random_url = generate_random_url(self.root, "tape_rest_api_enoent")
        files = [self.url, random_url] * 10

        sleep = 1
        status = 0
        while status == 0:
            (status, errors) = self.handle.archive_poll_list(files)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertEqual(status, 2)
        self.assertAllNone(errors[0::2])
        self.assertAllEqual([error.code for error in errors[1::2]], errno.ENOMSG)
