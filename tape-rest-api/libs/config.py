# Tape REST API tests configuration
import os

TapeBaseDirectory = os.environ.get('TAPE_ENDPOINT', "replacethis")
SourceFile = os.environ.get('TEST_FILE', 'file:///etc/hosts')

# Size of bulk requests
BulkSize = 10

# Max Polling interval in seconds
MaxPollInterval = 10

# Enable/disable verbose printing (log level = INFO)
Verbose = True

# Enable/disable debug printing (log level = DEBUG)
Debug = False