import gfal2
import uuid
import errno


def generate_random_url(root, filename):
    return root + filename + '_' + str(uuid.uuid4())


class GfalWrapper:

    def __init__(self, verbose=False, debug=False):
        self.context = gfal2.creat_context()
        if verbose or debug:
            gfal2_log_level = gfal2.verbose_level.debug if debug else gfal2.verbose_level.verbose
            gfal2.set_verbose(gfal2_log_level)

    def copy_file(self, source, destination, timeout, overwrite=False):
        params = self.context.transfer_parameters()
        params.timeout = timeout
        params.overwrite = overwrite
        return self.context.filecopy(params, source, destination)

    def release_list(self, files, token):
        return [self.context.release(file, token) for file in files]

    def rm_list(self, files):
        for file in files:
            self.context.unlink(file)

    def bring_online_poll_list(self, urls, token):
        errors = self.context.bring_online_poll(urls, token)
        error_count = 0
        online_count = 0

        for error in errors:
            if error is None:
                online_count += 1
            elif error.code != errno.EAGAIN:
                error_count += 1
        if error_count == len(errors):
            return -1, errors
        elif online_count == len(errors):
            return 1, errors
        elif (online_count + error_count) == len(errors):
            return 2, errors

        # Request still not finished
        return 0, errors

    def archive_poll_list(self, urls):
        errors = self.context.archive_poll(urls)
        error_count = 0
        ontape_count = 0

        for error in errors:
            if error is None:
                ontape_count += 1
            elif error.code != errno.EAGAIN:
                error_count += 1
        if error_count == len(errors):
            return -1, errors
        elif ontape_count == len(errors):
            return 1, errors
        elif (ontape_count + error_count) == len(errors):
            return 2, errors

        # Request still not finished
        return 0, errors
