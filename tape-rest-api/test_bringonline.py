#!/usr/bin/python3

import time
from test_case_base import *


class TestBringOnlineSingle(TestCaseBase):

    # Stage one single file
    def test_single_file(self):
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        while status == 0:
            status = self.handle.context.bring_online_poll(self.url, token)
        self.assertEqual(status, 1)

    # Stage a file that does not exist
    def test_single_file_enoent(self):
        file = generate_random_url(self.root, "tape_rest_api_enoent")
        (status, token) = self.handle.context.bring_online(file, 60, 60, False)
        self.assertEqual(status, 0)
        try:
            status = self.handle.context.bring_online_poll(file, token)
            self.assertEqual(status, -1)
        except Exception as e:
            self.assertEqual(e.code, errno.ENOMSG)

    # Stage bulk request
    def test_bulk_request(self):
        self._upload_and_register_files(self.bulk_size)
        (errors, token) = self.handle.context.bring_online(self.remote_files, 60, 60, False)
        sleep = 1
        status = 0
        while status == 0:
            print("Polling")
            (status, errors) = self.handle.bring_online_poll_list(self.remote_files, token)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, self.max_poll_interval)

        self.assertEqual(status, 1)
        self.assertAllNone(errors)

    # Stage bulk request (some files in the request do not exist)
    def test_bulk_request_enoent(self):
        files_enoent = [generate_random_url(self.root, "tape_rest_api_enoent") for _ in range(self.bulk_size)]
        self._upload_and_register_files(self.bulk_size)
        urls = [el for pair in zip(self.remote_files, files_enoent) for el in pair]
        (errors, token) = self.handle.context.bring_online(urls, 60, 60, False)

        sleep = 1
        status = 0
        while status == 0:
            print("Polling")
            (status, errors) = self.handle.bring_online_poll_list(urls, token)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, 10)

        self.assertEqual(status, 2)
        self.assertAllNone(errors[0::2])
        self.assertAllEqual([error.code for error in errors[1::2]], errno.ENOMSG)

    # Stage a list of files with duplicated entries
    def test_duplicates(self):
        random_url = generate_random_url(self.root, "tape_rest_api_enoent")
        files = [self.url, random_url] * 10
        (errors, token) = self.handle.context.bring_online(files, 60, 60, False)

        sleep = 1
        status = 0
        while status == 0:
            (status, errors) = self.handle.bring_online_poll_list(files, token)
            time.sleep(sleep)
            sleep *= 2
            sleep = min(sleep, 30)

        self.assertEqual(status, 2)
        self.assertAllNone(errors[0::2])
        self.assertAllEqual([error.code for error in errors[1::2]], errno.ENOMSG)

    # Poll with an invalid token
    def test_invalid_token(self):
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        try:
            status = self.handle.context.bring_online_poll(self.url, "abcde-12345")
            self.assertEqual(status, -1)
        except Exception as e:
            self.assertEqual(e.code, errno.EINVAL)

    # Release a file
    def test_release_file(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        while status == 0:
            status = self.handle.context.bring_online_poll(self.url, token)
        self.assertEqual(status, 1)
        # Release file
        status = self.handle.context.release(self.url, token)
        self.assertEqual(status, 0)

    # Release a file with invalid token
    def test_release_file_invalid_token(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        # Release file with wrong token is not an error
        status = self.handle.context.release(self.url, "abcde-12345")
        self.assertEqual(status, 0)

    #  Release a wrong file
    def test_release_wrong_file(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        # Release file that does not belong to the request ID
        random_url = generate_random_url(self.root, "tape_rest_api_enoent")
        status = self.handle.context.release(random_url, token)
        # Release file that does not belong to a given request id is not an error
        self.assertEqual(status, 0)

    # Abort a staging request
    def test_abort_request(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        # Abort request
        status = self.handle.context.abort_bring_online(self.url, token)
        self.assertEqual(status, 0)

    # Abort a staging request with wrong token
    def test_abort_request_invalid_token(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        # Abort request with wrong token
        try:
            status = self.handle.context.abort_bring_online(self.url, "abcde-12345")
            self.assertEqual(status, -1)
        except Exception as e:
            self.assertEqual(e.code, errno.EINVAL)

    # Abort a staging request with wrong file
    def test_abort_request_wrong_file(self):
        # Stage file to disk
        (status, token) = self.handle.context.bring_online(self.url, 60, 60, False)
        self.assertEqual(status, 0)
        # Abort request with wrong file
        try:
            random_url = generate_random_url(self.root, "tape_rest_api_enoent")
            status = self.handle.context.abort_bring_online(random_url, token)
            self.assertEqual(status, -1)
        except Exception as e:
            self.assertEqual(e.code, errno.EINVAL)


if __name__ == '__main__':
    unittest.main()
