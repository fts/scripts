import unittest
import logging
from tabnanny import verbose

from libs.gfal_helper import *
from libs import config


class TestCaseBase(unittest.TestCase):

    def setUp(self):
        self.handle = GfalWrapper(verbose=config.Verbose, debug=config.Debug)
        self.source = config.SourceFile
        self.root = config.TapeBaseDirectory
        self.bulk_size = config.BulkSize
        self.max_poll_interval = config.MaxPollInterval
        self.url = generate_random_url(self.root, "tape_rest_api")
        status = self.handle.copy_file(self.source, self.url, 60, False)
        self.assertEqual(status, 0)
        self.remote_files = [self.url, ]

    def tearDown(self):
        # Remove all files uploaded to the remote host
        self.handle.rm_list(self.remote_files)

    def _upload_and_register_files(self, n_files=1):
        src = [self.source]*n_files
        dst = []
        for i in range(n_files):
            dst.append(generate_random_url(self.root, "tape_rest_api"))
        errors = self.handle.copy_file(src, dst, 60, False)
        for err in errors:
            self.assertIsNone(err)
        self.remote_files += dst

    def assertAllEqual(self, seq, key):
        for el in seq:
            self.assertEqual(el, key)

    def assertAllNone(self, seq):
        for el in seq:
            self.assertIsNone(el)


if config.Debug or config.Verbose:
    logging_level = logging.DEBUG if config.Debug else logging.INFO
    logging.getLogger().setLevel(logging_level)
