# Tape REST API Testing

This directory contains a collection of GFAL2 tests that represent the set of operations that FTS3 will use when
interacting with a Tape REST API endpoint via GFAL2. The purpose of these tests is to test the integration of
FTS/GFAL2 with the Tape REST API. If this set of tests passes, we can assume that the integration works and that more advanced tests can be performed.
The testsuite requires python3-gfal2 v1.12.0 + gfal2 v2.21.0
(found in [DMC production repos](https://dmc-repo.web.cern.ch/dmc-repo/el7/x86_64/))


- Running the testsuite:
```shell
$ export TAPE_ENDPOINT=<url-to-test-endpoint-directory> # example: https://ctadevfts.cern.ch:8444/eos/ctaeos/preprod/gfal2_tests/
$ export TEST_FILE=<url-to-test-file> # example: file:///tmp/file.1mb
$ python3 -m pytest test_*.py
```

Note that you will need to have a valid X509 certificate to interact with the remote storage endpoint.
